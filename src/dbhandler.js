/* eslint no-console: "off" */
const nano = require('nano')('http://couchdb:5984');

nano.db.list((err, body) => {
  if (err) throw new Error(err);
  if (body.includes('collected_data')) {
    console.log('collected_data found. Exiting');
  } else {
    console.log('collected_data not found. Creating.');
    nano.db.create('collected_data', (inerr) => {
      if (err) throw new Error(inerr);
      console.log('collected_data created. Exiting.');
    });
  }
});
