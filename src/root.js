const db = require('nano')('http://couchdb:5984/collected_data');

function getById(id) {
  return new Promise((resolve, reject) => {
    db.get(id, (err, body) => {
      if (err) return reject(err);
      return resolve(body);
    });
  });
}

function insert(doc) {
  return new Promise((resolve, reject) => {
    db.insert(doc, (err, body) => {
      if (err) return reject(err);
      return resolve(body);
    });
  });
}


module.exports = {
  getById,
  insert,
};
