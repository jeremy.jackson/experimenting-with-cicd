/* eslint no-console: "off" */
const express = require('express');
const bodyParser = require('body-parser');

const { getById, insert } = require('./src/root.js');

const app = express();

app.use(bodyParser.json());

app.route('/')
  .get((req, res) => {
    getById(req.query.id)
      .then(passedData => res.send(passedData))
      .catch(passedErr => res.status(500).send(passedErr));
  })
  .post((req, res) => {
    insert(req.body)
      .then(passedData => res.send(passedData))
      .catch(passedErr => res.status(500).send(passedErr));
  });

app.listen(3000, () => console.log('listening on port 3000'));
