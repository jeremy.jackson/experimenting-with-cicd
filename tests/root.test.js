/* eslint no-unused-vars: "off" */
/* global describe, it, before, beforeEach, done */

const expect = require('expect.js');
const nano = require('nano')('http://couchdb:5984');

const {
  getById,
  insert,
} = require('../src/root.js');

describe('src/root.js', () => {
  describe('insert()', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('collected_data')) {
          nano.db.destroy('collected_data', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('collected_data', done);
          });
        } else {
          nano.db.create('collected_data', done);
        }
      });
    });

    it('should successfully insert bob\'s record into the database', (done) => {
      const insertTest = [
        {
          arg: { _id: 'bob', somedata: { one: 1, two: 'two' } },
          expect: ['ok', 'id', 'rev'],
        },
      ];

      insertTest.forEach((test) => {
        insert(test.arg)
          .then((returned) => {
            expect(returned).to.only.have.keys(test.expect);
            done();
          })
          .catch(err => done(err));
      });
    });
  });

  describe('getById()', () => {
    before((done) => {
      const bob = { _id: 'bob', somedata: { one: 1, two: 'two' } };
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('collected_data')) {
          nano.db.destroy('collected_data', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('collected_data', (cerr, cbody) => {
              if (cerr) throw new Error(err);
              const db = nano.use('collected_data');
              db.insert(bob, (berr, bbody) => {
                if (berr) throw new Error(berr);
                done();
              });
            });
          });
        } else {
          nano.db.create('collected_data', (cerr, cbody) => {
            if (cerr) throw new Error(err);
            const db = nano.use('collected_data');
            db.insert(bob, (berr, bbody) => {
              if (berr) throw new Error(berr);
              done();
            });
          });
        }
      });
    });

    it('should successfully return bob\'s record', (done) => {
      const getTest = [
        {
          arg: 'bob',
          expect: ['_id', '_rev', 'somedata'],
        },
      ];

      getTest.forEach((test) => {
        getById(test.arg)
          .then((returned) => {
            expect(returned).to.have.keys(test.expect);
            done();
          })
          .catch(err => done(err));
      });
    });
  });
});
