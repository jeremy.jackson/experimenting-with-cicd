FROM node:8
ADD . /code
WORKDIR /code
RUN npm install
RUN npm run lint
EXPOSE 3000
CMD ["bash", "launcher.sh"]
