[![pipeline status](https://gitlab.com/jeremy.jackson/experimenting-with-cicd/badges/master/pipeline.svg)](https://gitlab.com/jeremy.jackson/experimenting-with-cicd/commits/master)
[![coverage report](https://gitlab.com/jeremy.jackson/experimenting-with-cicd/badges/master/coverage.svg)](https://gitlab.com/jeremy.jackson/experimenting-with-cicd/commits/master)

# Experimenting with GitLab CI/CD
The goal is to learn how to do a full CI/CD pipeline in GitLab.

## Why use CI/CD?
CI/CD provides us with a bunch of benefits.
1. Provides unambiguous ground truth about when it's "done".
2. Can help us understand metrics about code quality and how we're doing at testing,
code quality, etc..
3. Can eventually be used to create build artifacts that can be easily deployed.
4. Can eventually DO THE DEPLOYMENTS FOR US.

## How do you keep GitLab's CI/CD up-to-date?
You have to become a little bit familiar with some of the ideas behind `docker-compose`.
So `docker-compose` lets you bring several containers together.
Ideally your project has one 'project' container and then relies on other containers
as 'services', such as databases.

GitLab CI/CD goes a couple steps beyond this by making it so that each container you
might have has a very short lifespan and serves a small number of purposes.
This lets you do things like focus one container on running tests, another on
running static analysis code quality tools, another to create a deployment artifact
(potentially another container, a built website, or a compiled binary), and yet another
to generate documentation.

To keep things up-to-date you need to keep in mind what you're working on in your code
and what is needed to keep your pipeline healthy.
You can start by adding testing to your project, and then adding both a `stage` and a
`job` to your `.gitlab-ci.yml` file for `test`ing.
You define what `docker` image you want to use for testing (for us that's `node:8`),
what other services need to be available (`couchdb:2.1.1`), and what commands need to
be run to make the tests run successfully.

To do local testing for debugging the containers to get them talk to each other you
can use `docker-compose` as noted below to get a better understanding of how your
project needs to be put together so that it will work in the CI/CD pipeline.
You do end up writing things a little differently...

Once you understand how the containers work together and what commands need to be run
to accomplish running your tests you'll have a pretty good idea of how to build your
`job` in the `.gitlab-ci.yml` file.

Then, once you push, you can go to the CI/CD tools in GitLab and see how you did.
If it fails, there's often enough information to understand why.
If it succeeds, you get the justification that you learned what you were doing enough
to make things actually happen.
And that always feels great.

As you add more things into your project, such as deployment scripts, code coverage
tools, etc., you need to make sure that you're appropriately either extending or
creating `jobs` to accomplish your goals.

## `docker`
Make sure that you have the Docker distribution for your operating system installed.

[Guide to install Docker](https://docs.docker.com/install/)

## `docker-compose`
We're using `docker-compose` for local building, testing, and running.

### `docker-compose` Prerequisites
Since `docker-compose` is a Python package, you should make sure that Python 2.7
(preferably not Anaconda) is installed.

Then make sure that you have the following packages installed:
- `pip`
- `pipenv`

These two packages will help you to make easier use of python's `virtualenv`,
and thus `docker-compose`.

`pipenv` is definitely the most important. [Here](https://docs.pipenv.org/) are the
docs.

`docker-compose` is listed in the `Pipfile`, so once you clone the package from
GitLab, you will get everything installed by runnning the following from within
the directory:

```bash
pipenv install
```

### Running `docker-compose`
To run `docker-compose` you need to have installed it (as above) by running:

```bash
pipenv install
```

Then you can enter the `virtualenv` that `pipenv` created for you by running

```bash
pipenv shell
```

You can exit the pipenv shell by just typing `exit`.

While you're inside this `virtualenv` you can run `docker-compose` very easily for
a variety of tasks:

```bash
# Build/get the docker images defined in the Dockerfile and docker-compose.yml
# You should do this if you've made some changes that you want to test out.
docker-compose build

# Launch all of the containers defined in docker-compose.yml
# The `-d` option makes it run in the background, otherwise you get the full
# stdout from both containers, which can be pretty chatty.
docker-compose up -d

# See the currently running containers and their statuses.
docker-compose ps

# Run all the tests and get code coverage report against the `web` container.
# The `web` container runs the node application we're building.
docker-compose run web npm run test-coverage

# Check out the logs generated by the currently running `web` container.
# This is useful for debugging.
# You can add the `-f` switch to follow the log as well.
docker-compose logs web

# Destroy all of the containers. They're still 'built', they've just been removed.
docker-compose down
```

## Deployment Strategy
1. Look into GitLab's Container Registry.
2. Look into running this as a docker container on either AWS or Digital Ocean.
3. Figure out how to do automated deploys.
