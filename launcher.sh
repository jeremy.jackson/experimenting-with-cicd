#!/bin/bash

until curl -s http://couchdb:5984
do
  sleep 1
done

node ./src/dbhandler.js
npm run start
